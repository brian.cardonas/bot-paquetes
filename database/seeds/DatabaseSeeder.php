<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        App\User::query()->delete();
        App\TipoPaquete::query()->delete();
        App\Envio::query()->delete();
        $this->call(UsersSeeder::class);
        $this->call(TiposPaqueteSeeder::class);
        $this->call(EnviosSeeder::class);
    }
}

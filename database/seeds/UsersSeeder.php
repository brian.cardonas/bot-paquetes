<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        User::create([
            'codigo' => 'ABC123', 
            'nombres' => 'Brian', 
            'administrador' => false,
        ]);

        User::create([
            'codigo' => 'DEF456', 
            'nombres' => 'Valentina', 
            'administrador' => false,
        ]);

        User::create([
            'codigo' => 'GHI789', 
            'nombres' => 'Cristian', 
            'administrador' => true,
        ]);

        User::create([
            'codigo' => 'JKL123', 
            'nombres' => 'Diego', 
            'administrador' => true,
        ]);

        //Usuario para pruebas
        User::create([
            'codigo' => '1', 
            'nombres' => 'Marcel', 
            'administrador' => true,
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\TipoPaquete;

class TiposPaqueteSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        TipoPaquete::create([
            'nombre' => 'Documentos',
            'activo' => true
        ]);

        TipoPaquete::create([
            'nombre' => 'Orgánico',
            'activo' => false
        ]);

        TipoPaquete::create([
            'nombre' => 'Ropa',
            'activo' => true
        ]);

        TipoPaquete::create([
            'nombre' => 'Juguetes',
            'activo' => true
        ]);

        TipoPaquete::create([
            'nombre' => 'Tecnología',
            'activo' => false
        ]);

        TipoPaquete::create([
            'nombre' => 'Otros',
            'activo' => true
        ]);
    }
}

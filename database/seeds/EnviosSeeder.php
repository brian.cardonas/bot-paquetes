<?php

use Illuminate\Database\Seeder;
use App\Envio;
use App\Paquete;

class EnviosSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $usuario = App\User::first();

        $envio = Envio::create([
            'remitente' => $usuario->nombres,
            'direccion_remitente' => 'Cra 6C #15A-21',
            'destinatario' => 'Carlos Iván Valencia',
            'direccion_destinatario' => 'Acuarelas de Campo Hermoso, casa D28',
            'fecha_envio' => null,
            'estado' => 'Pendiente', 
            'usuario_id' => $usuario->id,
            'codigo_envio' => '20191704_1'
        ]);

        Paquete::create([
            'peso' => 2.25,
            'tipo_paquete_id' => App\TipoPaquete::first()->id,
            'envio_id' => $envio->id,
        ]);

        Paquete::create([
            'peso' => 3.00,
            'tipo_paquete_id' => App\TipoPaquete::first()->id,
            'envio_id' => $envio->id,
        ]);

        $usuario = App\User::where('nombres', 'Valentina')->first();

        $envio = Envio::create([
            'remitente' => $usuario->nombres,
            'direccion_remitente' => 'Cra 6C #15A-21',
            'destinatario' => 'Valentina Londoño Marín',
            'direccion_destinatario' => 'CL 15 24 52 San Antonio',
            'fecha_envio' => '2019-04-01',
            'estado' => 'Recogido', 
            'usuario_id' => $usuario->id,
            'codigo_envio' => '20191704_2'
        ]);

        Paquete::create([
            'peso' => 2.25,
            'tipo_paquete_id' => App\TipoPaquete::where('nombre', 'Documentos')->first()->id,
            'envio_id' => $envio->id,
        ]);

        Paquete::create([
            'peso' => 7.30,
            'tipo_paquete_id' => App\TipoPaquete::where('nombre', 'Orgánico')->first()->id,
            'envio_id' => $envio->id,
        ]);

        Paquete::create([
            'peso' => 5.10,
            'tipo_paquete_id' => App\TipoPaquete::where('nombre', 'Ropa')->first()->id,
            'envio_id' => $envio->id,
        ]);

        Paquete::create([
            'peso' => 1.50,
            'tipo_paquete_id' => App\TipoPaquete::where('nombre', 'Otros')->first()->id,
            'envio_id' => $envio->id,
        ]);


        //Envios asociados a Marcel para pruebas con PHP Unit
        $usuario = App\User::where('codigo',1)->first();

        $envio = Envio::create([
            'remitente' => $usuario->nombres,
            'direccion_remitente' => 'Carrera 1 # 2 -3',
            'destinatario' => 'Amigo de Marcel',
            'direccion_destinatario' => 'Carrera 4 # 5 - 6',
            'fecha_envio' => '2019-04-06',
            'estado' => 'En Camino', 
            'usuario_id' => $usuario->id,
            'codigo_envio' => '20191704_3'
        ]);

        Paquete::create([
            'peso' => 4,
            'tipo_paquete_id' => App\TipoPaquete::first()->id,
            'envio_id' => $envio->id,
        ]);

        $envio = Envio::create([
            'remitente' => $usuario->nombres,
            'direccion_remitente' => 'Carrera 1 # 2 -3',
            'destinatario' => 'Mama de Marcel',
            'direccion_destinatario' => 'Carrera 7 # 8 - 9',
            'fecha_envio' => '2019-04-01',
            'estado' => 'Recogido', 
            'usuario_id' => $usuario->id,
            'codigo_envio' => '20191704_4'
        ]);

        Paquete::create([
            'peso' => 20,
            'tipo_paquete_id' =>  App\TipoPaquete::where('nombre', 'Ropa')->first()->id,
            'envio_id' => $envio->id,
        ]);

        Paquete::create([
            'peso' => 6,
            'tipo_paquete_id' => App\TipoPaquete::where('nombre', 'Juguetes')->first()->id,
            'envio_id' => $envio->id,
        ]);
    }
}

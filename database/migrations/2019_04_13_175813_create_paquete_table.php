<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaqueteTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('paquete', function (Blueprint $table) {
            $table->increments('id')->comment('ID');
            $table->double('peso', 8, 2)->comment('Peso');
            $table->unsignedInteger('tipo_paquete_id')->comment('Tipo');
            $table->unsignedInteger('envio_id')->comment('Envío');
            $table->timestamps();

        });

        Schema::table('paquete', function ($table) {
            $table->foreign('tipo_paquete_id')->references('id')->on('tipo_paquete')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('envio_id')->references('id')->on('envio')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('Paquetes');
    }
}

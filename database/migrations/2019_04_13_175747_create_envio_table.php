<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnvioTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('envio', function (Blueprint $table) {
            $table->increments('id')->comment('ID');
            $table->string('remitente')->comment('Remitente');
            $table->string('direccion_remitente', 255)->nullable()->comment('Dirección del remitente'); 
            $table->string('destinatario')->nullable()->comment('Destinatario');
            $table->string('direccion_destinatario', 255)->nullable()->comment('Dirección del destinatario');
            $table->date('fecha_envio')->nullable()->comment('Fecha del envío');
            $table->enum('estado', ['Pendiente', 'Recogido','En camino','Entregado'])->comment('Estado');
            $table->unsignedInteger('usuario_id')->comment('ID del Usuario');
            $table->string('codigo_envio', 255)->nullable()->comment('Código autogenerado del envío');
            $table->timestamps();
        });

        Schema::table('envio', function ($table) {
            $table->foreign('usuario_id')->references('id')->on('users')
                ->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('envio');
    }
}

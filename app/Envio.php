<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Envio extends Model {
    protected $table = "envio";

    const ESTADO_PENDIENTE = 'Pendiente'; 

    const ESTADO_RECOGIDO = 'Recogido';

    const ESTADO_EN_CAMINO = 'En camino';

    const ESTADO_ENTREGADO = 'Entregado';

    //Se establecen los campos que se pueden editar del modelo
    protected $fillable = [
        'remitente','direccion_remitente','destinatario','direccion_destinatario',
        'fecha_envio','estado', 'usuario_id','codigo_envio'
    ];

    //Un envío puede contener muchos paquetes. 
    public function paquetes()
    {
        return $this->hasMany('App\Paquete');
    }

    //Un envío estará vincunlado a un sólo usuario 
    public function usuario()
    {
        return $this->belongsTo('App\User');
    }

}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    //Token que confirmará que un usuario es admin
    const TOKEN_ADMIN = '@123'; 

    //Se establecen los campos que se pueden editar del modelo
    protected $fillable = [
        'codigo', 'nombres', 'administrador',
    ];
}

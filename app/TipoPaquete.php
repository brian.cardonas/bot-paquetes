<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPaquete extends Model {
    protected $table = "tipo_paquete";

    const ESTADO_ACTIVO = "Activo";
    const ESTADO_INACTIVO = "Inactivo";

    //Se establecen los campos que se pueden editar del modelo
    protected $fillable = [
        'nombre', 'activo'
    ];

    //Un tipo de paquete puede estar asociado a muchos paquetes. 
    public function paquetes() {
        return $this->hasMany('App\Paquete');
    }
}

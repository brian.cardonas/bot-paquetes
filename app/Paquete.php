<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paquete extends Model {
    protected $table = "paquete";

    //Se establecen los campos que se pueden editar del modelo
    protected $fillable = [
        'peso','tipo_paquete_id', 'envio_id', 'usuario_id'
        ];

    //Un paquete estará vinculado a solo un tipo de paquete. 
    public function tipoPaquete()
    {
        return $this->belongsTo('App\TipoPaquete');
    }

    //Un paquete estará vinculado a solo un envío. 
    public function envio()
    {
        return $this->belongsTo('App\Envio');
    }

}

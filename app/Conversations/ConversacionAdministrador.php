<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use Illuminate\Support\Facades\Log;

use App\User;
use App\Envio;
use App\TipoPaquete;
use App\Paquete;

/**
 * Clase que representa una conversación con un administrador. El administrador
 * podrá: administrar tipos de paquetes, Consultar envíos y modificar su estado.
 * 
 * @author Cristian Salazar <cristian.salazarp@autonoma.edu.co>
 * @version 20190417
 */
class ConversacionAdministrador extends Conversation
{
    protected $usuario;
    protected $envioActualizar;
    protected $tipoPaqueteConsultar;

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run() {
        $this->confirmarToken();        
    }
    
    
    /**
     * Confirma si el usuario conoce el token de Admin 
     * En caso contrario lo remite a otras opciones
     */
    private function confirmarToken() {        
        $usuarioBot = $this->bot->getUser();
        $id = $usuarioBot->getId();
        $nombres = $usuarioBot->getFirstName() ?: "Desconocido";

        $esAdmin = $this->confirmarAdmin($id);
        //En caso de ser un Admin previamente registrado no solicita el token
        if(!$esAdmin){
            $this->ask("Ingresa el token que confirma que eres admin", function (Answer $respuesta) {
                if($respuesta->getText() == User::TOKEN_ADMIN) {    
                    $usuarioBot = $this->bot->getUser();
                    $id = $usuarioBot->getId();
                    $nombres = $usuarioBot->getFirstName() ?: "Desconocido";           
                    $this->say("Bienvenido $nombres al SRSP.");
                    $this->cargarSesion($id,$nombres);
                    $this->mostrarOpciones();
                } else {
                    $this->say("El token ingresado no es válido. \n
                    Si deseas intentar de nuevo escribe /inicioAdmin. \n
                    Si lo que quieres usar las opciones de cliente escribe /inicioCliente");
                }
            }); 
        }else{
            $this->say("Bienvenido $nombres al SRSP.");
            $this->mostrarOpciones();
        }
    }


    /**
     * Confirma si el usuario es un Admin previamente registrado
     */
    private function confirmarAdmin($id) {       
        $usuario = User::where('codigo',$id)->where('administrador',true)->first();
        if($usuario!=null){
            $this->usuario= $usuario;
            return true;
        }else{
            return false;
        }
    }


    /**
     * Carga los valores iniciales del usuario para la conversación.
     * En caso de que exista previamente y no este como Admin lo modifica
     */
    private function cargarSesion($id,$nombres) {       
        $usuario = User::firstOrNew(array(
            'codigo' => $id,
            'nombres' => $nombres,
        ));
        $usuario->administrador=true;
        $usuario->save();
        $this->usuario= $usuario;
    }

     /**
     * Muestra de manera interactiva las posibles opciones que tiene un administrador.
     */
    private function mostrarOpciones() {
        $opciones = Question::create("¿Qué acción desea realizar?")
            ->addButtons([
                Button::create('Administrar Tipos de Paquetes')->value(1),
                Button::create('Administrar Envíos')->value(2),
                Button::create('Salir')->value(3),
            ]);
        $this->ask($opciones, function (Answer $respuesta) {
            if ($respuesta->isInteractiveMessageReply()) {
                $opcion = $respuesta->getValue();

                if($opcion == 1) {
                    $this->say('Entendido, comencemos.');
                    //Espacio para historia de usuario 07   
                    $this->mostrarOpcionesAdminTipoPaquetes();            
                }
                if($opcion == 2) {
                    $this->say('Entendido, comencemos.');
                    $this->mostrarOpcionesEnvios();
                }
                if($opcion == 3) {
                    $this->say('Gracias por utilizar nuestros servicios.');
                    return true;
                }
            } else {
                $this->say('Por favor elige una opción de la lista.');
                $this->mostrarOpciones();
            }
        });
    }

    /**
     * Muestra de manera interactiva las opciones que tiene un administrador sobre los envíos.
     */
    private function mostrarOpcionesEnvios() {
        $opciones = Question::create("Seleccione alguna de las siguientes opciones para los envíos")
            ->addButtons([
                Button::create('1. Modificar estado de envío por listado')->value(1),
                Button::create('2. Modificar estado de envío por código')->value(2),
                Button::create('3. Listar todos los envíos')->value(3),
                Button::create('4. Volver')->value(4)
            ]);
        $this->ask($opciones, function (Answer $respuesta) {
            if ($respuesta->isInteractiveMessageReply()) {
                $opcion = $respuesta->getValue();

                if($opcion == 1) {
                    $this->preguntarEnvio();
                }

                if($opcion == 2) {
                    $this->preguntarCodigoEnvio();
                }
                
                if($opcion == 3) {
                    $this->consultarEnvios();
                }
                
                if($opcion == 4) {
                    $this->mostrarOpciones();
                }
            } else {
                $this->say('Por favor elige una opción de la lista.');
                $this->mostrarOpcionesEnvios();
            }
        });
    }    

    /**
     * Método que lista todos los envíos que aún no han sido entregados para cambiar su estado
     * con sólo seleccionarlo.
     */
    private function preguntarEnvio() {
        $envios = Envio::where('estado', '!=', Envio::ESTADO_ENTREGADO)->orderBy('fecha_envio')->get();
        if($envios->isNotEmpty()) {
            $this->say('Entendido, continuemos.');
            $botones = [];
            
            foreach($envios as $envio) {
                $etiqueta = $envio->codigo_envio . ' - ' . $envio->estado;
                $botones[] = Button::create($etiqueta)->value($envio->id);
            }
            
            $opciones = Question::create("Seleccione el envío al que desea cambiar su estado: ")->addButtons($botones);
            
            $this->ask($opciones, function (Answer $respuesta) {
                if($respuesta->isInteractiveMessageReply()) {
                    $this->envioActualizar = Envio::find($respuesta->getValue());
                    $this->cambiarEstadoEnvio();
                    $this->say($this->mensajeActualizacion());
                    $this->enviarMensajeCliente();
                    $this->mostrarOpcionesEnvios();
                } else {
                    $this->say('Por favor elige una opción de la lista.');
                    $this->preguntarEnvio();
                }
            });
        } else {
            $this->say('Lo sentimos :( no hay envíos disponibles en el Sistema.');
        }
    }

    /**
     * Método que Pregunta el código del envío a actualizar y muestra la información 
     * relacionado a este.
     */
    private function preguntarCodigoEnvio() {
        $this->ask("Ingrese el código del envío a actualizar", function (Answer $respuesta) {
            
            if($respuesta != null && $respuesta != ''){

                $this->envioActualizar = Envio::where('codigo_envio', $respuesta)->first();

                if($this->envioActualizar && $this->envioActualizar->estado != Envio::ESTADO_ENTREGADO) {
                   
                    $this->cambiarEstadoEnvio();
                    
                    $this->say($this->mensajeActualizacion());
                    
                    //NOTIFICAR AL CLIENTE SOBRE EL CAMBIO DE ESTADO.
                    $this->enviarMensajeCliente();
                    
                    $this->mostrarOpcionesEnvios();

                }else if($this->envioActualizar->estado == Envio::ESTADO_ENTREGADO){
                    $this->say('El código de envío ingresado  se encuentra en estado entregado. No es modificable');
                    $this->mostrarOpcionesEnvios();

                }else {
                    $this->say('El código ingresado no es válido. Por favor intenta nuevamente.');
                    $this->preguntarCodigoEnvio();
                }
            }
            else{
                $this->say('El código del envío es obligatorio. Por favor intenta nuevamente.');
                $this->preguntarCodigoEnvio();
            }
        });
    }

    /**
     * Cambia el estado de un envío dependiendo su estado actual.
     */
    private function cambiarEstadoEnvio() {
        switch ($this->envioActualizar->estado) {
            case Envio::ESTADO_PENDIENTE:
                $this->envioActualizar->estado = Envio::ESTADO_RECOGIDO;
                break;
            case Envio::ESTADO_RECOGIDO:
                $this->envioActualizar->estado = Envio::ESTADO_EN_CAMINO;
                break;
            case Envio::ESTADO_EN_CAMINO:
                $this->envioActualizar->estado = Envio::ESTADO_ENTREGADO;
                break;
        }
        $this->envioActualizar->save();
    }
    /**
     * Mensaje que se utilizara para mostrar al administrador y al cliente cuando se cambia un estado.
     */
    public function mensajeActualizacion(){
        $mensaje = "Se ha actualizado el envío {$this->envioActualizar->codigo_envio} con los siguientes datos: \n";
        $mensaje = "--------Información actual del Envío---- \n";
        $mensaje .= "Nuevo estado: {$this->envioActualizar->estado}\n";
        $mensaje .= "Remitente: {$this->envioActualizar->remitente}\n";
        $mensaje .= "Dirección Rem: {$this->envioActualizar->direccion_remitente}\n";
        $mensaje .= "Destinatario: {$this->envioActualizar->destinatario}\n";
        $mensaje .= "Dirección Des: {$this->envioActualizar->direccion_destinatario}\n";
        $mensaje .= "Fecha del envío: {$this->envioActualizar->fecha_envio}\n";
        $mensaje .= "---------------------------------------";
        return $mensaje;
    }

    /**
     * Método que permite enviar un mensaje al cliente sobre la actualización
     * del estado de un envío.
     */
    private function enviarMensajeCliente(){
        $usuario = User::find($this->envioActualizar->usuario_id);
        $this->bot->say("Buen día Sr(a) ".$usuario->nombres, $usuario->codigo);    
        $this->bot->say($this->mensajeActualizacion(),$usuario->codigo);     
    }

    /**
     * Método que permite listar todos los envíos para ver su información.
     */
    private function consultarEnvios(){
        $envios = Envio::orderBy('estado')->get();
        if($envios->isNotEmpty()) {
            $this->say('Entendido, continuemos.');
            $botones = [];
            
            foreach($envios as $envio) {

                $label = $envio->codigo_envio . ' - ' . $envio->estado;

                $botones[] = Button::create($label)->value($envio->id);
            }
            
            $opciones = Question::create("Seleccione el envío: ")->addButtons($botones);
            
            $this->ask($opciones, function (Answer $respuesta) {

                if ($respuesta->isInteractiveMessageReply()) {

                    $envioConsultar = Envio::find($respuesta->getValue());


                    $mensaje = "--------Información actual del Envío---- \n";
                    $mensaje .= "Código envío: {$envioConsultar->codigo_envio} \n";
                    $mensaje .= "Estado actual: {$envioConsultar->estado}\n";
                    $mensaje .= "Remitente: {$envioConsultar->remitente}\n";
                    $mensaje .= "Dirección Rem: {$envioConsultar->direccion_remitente}\n";
                    $mensaje .= "Destinatario: {$envioConsultar->destinatario}\n";
                    $mensaje .= "Dirección Des: {$envioConsultar->direccion_destinatario}\n";
                    $mensaje .= "Fecha del envío: {$envioConsultar->fecha_envio}\n";
                    $mensaje .= "------Información de Paquetes---- \n";

                    $paquetes = Paquete::where('envio_id', $envioConsultar->id)->orderBy('id')->get();

                    foreach($paquetes as $paquete) {
                        
                        $mensaje .= 'Tipo de Paquete : ' . $paquete->tipoPaquete->nombre . "\n";
                        $mensaje .= 'Peso: ' . $paquete->peso . "\n";
                        $mensaje .= "------------------------------ \n";
                    }

                    $this->say($mensaje);
                    $this->mostrarOpcionesEnvios();
                } else {
                    $this->say('Por favor elige una opción de la lista.');
                    $this->consultarEnvios();
                }
            });
        } else {
            $this->say('Lo sentimos :( aún no hay envíos disponibles en el Sistema.');
        }
    }

     /**
     * Método que muestra el menú de opciónes para administrar los tipos de paquetes.
     * @author Cristian Salazar <cristian.salazarp@autonoma.edu.co>
     * @version 20190510
     */
    private function mostrarOpcionesAdminTipoPaquetes(){
        $opciones = Question::create("Seleccione alguna de las siguientes opciones para los Tipos de paquetes:")
            ->addButtons([
                Button::create('1. Listar existentes')->value(1),
                Button::create('2. Crear tipo de paquete')->value(2),
                Button::create('3. Regresar')->value(3)
            ]);
        $this->ask($opciones, function (Answer $respuesta) {
            if ($respuesta->isInteractiveMessageReply()) {
                $opcion = $respuesta->getValue();

                if($opcion == 1) {
                    $this->listarTiposPaquetesExistentes();
                }
                
                if($opcion == 2) {
                    $this->crearTipoPaquete();
                }
                
                if($opcion == 3) {
                    $this->mostrarOpciones();
                }
            } else {
                $this->say('Por favor elige una opción de la lista.');
                $this->mostrarOpcionesAdminTipoPaquetes();
            }
        });
    }

    /**
     * Método que lista todos los tipos de paquetes disponibles para su administración.
     * @author Cristian Salazar <cristian.salazarp@autonoma.edu.co>
     * @version 20190510
     */
    private function listarTiposPaquetesExistentes(){
        $tiposPaquetes = TipoPaquete::orderBy('id')->get();

        if($tiposPaquetes->isNotEmpty()) {
            $botones = [];    
            foreach($tiposPaquetes as $tipoPaquete) {
                $etiqueta = $tipoPaquete->id . ' - ' . $tipoPaquete->nombre . ' - ' . ($tipoPaquete->activo 
                    ? TipoPaquete::ESTADO_ACTIVO : TipoPaquete::ESTADO_INACTIVO);
                $botones[] = Button::create($etiqueta)->value($tipoPaquete->id);
            }
            
            $botones[] = Button::create('Volver al menú anterior')->value('Volver');

            $opciones = Question::create("Se encuentran los siguientes tipos de paquetes, seleccione el que desee administrar:")
                ->addButtons($botones);
            
            $this->ask($opciones, function (Answer $respuesta) {
                if($respuesta->isInteractiveMessageReply()) {
                    $this->tipoPaqueteConsultar = TipoPaquete::find($respuesta->getValue());

                    if($respuesta->getValue() != 'Volver'){
                        $this->mostrarOpcionesTipoPaquete();
                    }
                    else{
                        $this->mostrarOpcionesAdminTipoPaquetes();
                    }           
                } else {
                    $this->say('Por favor elige una opción de la lista.');
                    $this->listarTiposPaquetesExistentes();
                }
            });
        } else {
            $this->say('Lo sentimos :( aún no hay tipos de paquetes disponibles en el Sistema.');
            $this->mostrarOpcionesAdminTipoPaquetes();
        }
    }

    /**
     * Método que muestra el menú de acciones a realizar para tipo de paquete seleccionado.
     * @author Cristian Salazar <cristian.salazarp@autonoma.edu.co>
     * @version 20190510
     */
    private function mostrarOpcionesTipoPaquete(){

        $opciones = Question::create("Seleccione la acción a realizar para el tipo de paquete:")
            ->addButtons([
                Button::create('1. Editar')->value(1),
                Button::create('2. Activar/inactivar')->value(2),
                Button::create('3. Regresar')->value(3)
            ]);
        $this->ask($opciones, function (Answer $respuesta) {
            if ($respuesta->isInteractiveMessageReply()) {
                $opcion = $respuesta->getValue();

                if($opcion == 1) {
                    $this->preguntarNombreTipoPaquete();
                }
                
                if($opcion == 2) {
                    $this->cambiarEstadoTipoPaquete();
                }
                
                if($opcion == 3) {
                    $this->listarTiposPaquetesExistentes();
                }
            } else {
                $this->say('Por favor elige una opción de la lista.');
                $this->mostrarOpcionesTipoPaquete();
            }
        });
    }

    /**
     * Método que pregunta el nombre del tipo de paquete para modificar el tipo de paquete seleccionado.
     * @author Cristian Salazar <cristian.salazarp@autonoma.edu.co>
     * @version 20190510
     */
    private function preguntarNombreTipoPaquete(){
        $this->ask('Ingrese el nuevo nombre para el tipo del paquete:', function(Answer $respuesta) {
            if(!empty($respuesta->getText())) {
                $this->tipoPaqueteConsultar->nombre = $respuesta->getText();
                $this->tipoPaqueteConsultar->save();
                $this->say('Se ha modificado exitosamente el tipo de paquete.');
                $this->listarTiposPaquetesExistentes();
            } 
            else{
                $this->say('Debe ingresar un nombre válido, intentelo nuevamente.');
                $this->preguntarNombreTipoPaquete();
            }
        });
    }

    /**
     * Método que actualiza el estado de un tipo de paquete seleccionado.
     * Si está en activo lo cambia a inactivo y viceverza.
     * @author Cristian Salazar <cristian.salazarp@autonoma.edu.co>
     * @version 20190510
     */
    private function cambiarEstadoTipoPaquete(){
        $this->tipoPaqueteConsultar->activo = ($this->tipoPaqueteConsultar->activo ? false : true);
        $this->tipoPaqueteConsultar->save();
        $this->say('El nuevo estado del tipo de paquete es: ' . ($this->tipoPaqueteConsultar->activo 
            ? TipoPaquete::ESTADO_ACTIVO : TipoPaquete::ESTADO_INACTIVO));
        $this->listarTiposPaquetesExistentes();
    }

    /**
     * Método que permite crear un tipo de paquete.
     * @author Cristian Salazar <cristian.salazarp@autonoma.edu.co>
     * @version 20190510
     */
    private function crearTipoPaquete(){
        $this->ask('Ingrese el nombre para el tipo del paquete:', function(Answer $respuesta) {
            $nombreTmp = $respuesta->getText();
            if(!empty($nombreTmp)) {
                if($this->guardarTipoPaquete($nombreTmp) != null) {
                    $this->say('Se ha creado el tipo de paquete ' . $nombreTmp);
                } else {
                    $this->say('Hubo un problema al crear el tipo de paquete ' . $nombreTmp);
                }
                $this->mostrarOpcionesAdminTipoPaquetes();
            } else {
                $this->say('Debe ingresar un nombre, intentelo nuevamente.');
                $this->crearTipoPaquete();
            }
        });
    }

    /**
     * Guarda y valida que el tipo de paquete quede en la base de datos.
     * 
     * @param string $nombre nombre del tipo de paquete
     * @return boolean true si se creó correctamente, false de lo contrario
     */
    private function guardarTipoPaquete($nombre) {
        $tipoPaquete = null;
        $paqueteTmp = TipoPaquete::where('nombre', $nombre)->first();

        if(!$paqueteTmp) {
            $tipoPaquete = TipoPaquete::create([
                'nombre' => $nombre,
                'activo' => true
            ]);
        }
        return $tipoPaquete;
    }

}

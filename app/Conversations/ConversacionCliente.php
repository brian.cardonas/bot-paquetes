<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use Illuminate\Support\Facades\Log;

use App\User;
use App\Envio;
use App\TipoPaquete;
use App\Paquete;

/**
 * Clase que representa una conversación con un cliente. El cliente
 * podrá registrar envíos y consultarlos posteriormente.
 * 
 * @author Brian Cardona <brian.cardonas@autonoma.edu.co>
 * @version 20190414
 */
class ConversacionCliente extends Conversation {

    protected $usuario;
    protected $envio;

    protected $tipoPaquete;
    protected $pesoPaquete;

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run() {
        $this->cargarSesion();
        $this->mostrarOpciones();
    }

    /**
     * Carga los valores iniciales del usuario para la conversación.
     */
    private function cargarSesion() {
        $usuarioBot = $this->bot->getUser();
        $id = $usuarioBot->getId();
        $this->usuario = User::where('codigo', $id)->first();
        $this->envio = null;
    }

    /**
     * Muestra de manera interactiva las posibles opciones que tiene un cliente.
     */
    private function mostrarOpciones() {
        $opciones = Question::create("¿Qué acción desea realizar?")
            ->addButtons([
                Button::create('Registrar nuevo envío')->value(1),
                Button::create('Consultar el estado de un envío')->value(2),
                Button::create('Consultar histórico de envíos')->value(3),
                Button::create('Salir')->value(4),
            ]);
        $this->ask($opciones, function (Answer $respuesta) {
            if ($respuesta->isInteractiveMessageReply()) {
                $opcion = $respuesta->getValue();

                if($opcion == 1) {
                    $this->say('Entendido, comencemos.');
                    $this->prepararEnvio();
                    $this->mostrarOpcionesPaquetes();
                }
                if($opcion == 2) {
                    $this->historicoEnvios(true);
                }
                if($opcion == 3) {
                    $this->historicoEnvios();
                }
                if($opcion == 4) {
                    $this->say('Gracias por utilizar nuestros servicios.');
                    return true;
                }
            } else {
                $this->say('Por favor elige una opción de la lista.');
                $this->mostrarOpciones();
            }
        });
    }

    /**
     * Inicia un envío con la información mínima, para que el cliente
     * pueda registar paquetes sobre él y luego, sea formalizado para su envío.
     */
    private function prepararEnvio() {
        
        $envioPrevio = Envio::where("usuario_id",$this->usuario->id)->where("destinatario",null)->first();
        
        if(!empty($envioPrevio)){
            $this->envio = $envioPrevio;    
        }
        else{
            $this->envio = Envio::create([
                'remitente' => $this->usuario->nombres,
                'estado' => Envio::ESTADO_PENDIENTE,
                'usuario_id' => $this->usuario->id
            ]);
        }
    }

    /**
     * Muestra de manera interactiva las opciones que tiene un cliente sobre los paquetes.
     */
    private function mostrarOpcionesPaquetes() {
        $opciones = Question::create("Seleccione alguna de las siguientes opciones para los paquetes")
            ->addButtons([
                Button::create('Registrar paquete')->value(1),
                Button::create('Eliminar paquete')->value(2),
                Button::create('Confirmar envío')->value(3),
                Button::create('Volver')->value(4)
            ]);
        $this->ask($opciones, function (Answer $respuesta) {
            if ($respuesta->isInteractiveMessageReply()) {
                $opcion = $respuesta->getValue();

                if($opcion == 1) {
                    $this->preguntarTipoPaquete();
                }

                if($opcion == 2) {
                    $this->retirarPaquete();
                }
                
                if($opcion == 3) {
                    $this->confirmarEnvio();
                }
                
                if($opcion == 4) {
                    $this->validarEnvio();
                    $this->mostrarOpciones();
                }
            } else {
                $this->say('Por favor elige una opción de la lista.');
                $this->mostrarOpcionesPaquetes();
            }
        });
    }

    /**
     * Pregunta el tipo de paquete con base en los tipos disponibles.
     */
    private function preguntarTipoPaquete() {
        //Consultamos únicamente los tipos de paquetes activos
        $tiposPaquete = TipoPaquete::where('activo', 1)->orderBy('nombre')->get();
        if($tiposPaquete->isNotEmpty()) {
            $this->say('Entendido, continuemos.');
            $botones = [];
            foreach($tiposPaquete as $tipoPaquete) {
                $botones[] = Button::create($tipoPaquete->nombre)->value($tipoPaquete->id);
            }
            $opciones = Question::create("Seleccione el tipo del paquete")->addButtons($botones);
            $this->ask($opciones, function (Answer $respuesta) {
                if ($respuesta->isInteractiveMessageReply()) {
                    $this->tipoPaquete = $respuesta->getValue();
                    $this->preguntarPeso();
                } else {
                    $this->say('Por favor elige una opción de la lista.');
                    $this->preguntarTipoPaquete();
                }
            });
        } else {
            $this->say('Lo sentimos :( aún no hay tipos de paquetes disponibles en el Sistema. Por favor comuníquese con el Administrador');
        }
    }

    /**
     * Pregunta el valor del peso del paquete.
     */
    private function preguntarPeso() {
        $this->ask("Ingrese el peso del paquete en Kilogramos \n Ejemplo (3.5) hasta 2 decimales", function (Answer $respuesta) {
            if($this->validarPeso($respuesta->getText())) {
                $this->pesoPaquete = $respuesta->getText();
                $this->guardarPaquete();
            } else {
                $this->say('El peso ingresado no es válido. Por favor intenta nuevamente.');
                $this->preguntarPeso();
            }
        });
    }

    /**
     * Valida que el peso ingresado sea válido utilizando expresiones regulares.
     */
    private function validarPeso($respuesta) {
        return preg_match("/^[0-9]{0,5}+([.][0-9]{0,2}+)?$/", $respuesta);
    }

    /**
     * Guarda el paquete en la base de datos asociado al envío actual.
     */
    private function guardarPaquete() {
        $paquete = Paquete::create([
            'peso' => $this->pesoPaquete,
            'tipo_paquete_id' => $this->tipoPaquete,
            'envio_id' => $this->envio->id
        ]);

        if($paquete) {
            $this->say('Paquete registrado con éxito.');
        } else {
            $this->say('Hubo un problema al registrar el paquete.');
        }
        $this->say('¿Quieres hacer algo más?');
        $this->mostrarOpcionesPaquetes();
    }

    /**
     * Si no hay paquetes asociados al envío actual, será descartado.
     */
    private function validarEnvio() {
        if($this->envio->paquetes->isEmpty()) {
            $this->envio->delete();
        }
    }

    /**
     * Permite retirar un paquete del envío.
     */
    private function retirarPaquete() {
        $this->envio->refresh();
        if($this->envio->paquetes->isEmpty()) {
            $this->mostrarOpcionesEnvioVacio();
        } else {
            $this->preguntarPaqueteARetirar();
        }
    }

    /**
     * Pregunta de manera interactiva el paquete que se desea eliminar del envío actual.
     */
    private function preguntarPaqueteARetirar() {
        $paquetes = $this->envio->paquetes;
        $botones = [];
        foreach($paquetes as $paquete) {
            $botones[] = Button::create($paquete->tipoPaquete->nombre." ({$paquete->peso} kg)")->value($paquete->id);
        }
        $opciones = Question::create('Seleccione el paquete que desea retirar del envío actual')->addButtons($botones);
        $this->ask($opciones, function (Answer $respuesta) {
            if ($respuesta->isInteractiveMessageReply()) {
                $this->eliminarPaquete($respuesta->getValue());
            } else {
                $this->say('Por favor elige una opción de la lista.');
                $this->preguntarPaqueteARetirar();
            }
        });
    }

    /**
     * Elimina el registro del paquete en base de datos.
     * @param $idPaquete ID del paquete a eliminar
     */
    private function eliminarPaquete($idPaquete) {
        $paquete = Paquete::find($idPaquete);
        if($paquete->delete()) {
            $this->say('Paquete retirado del envío con éxito.');
        } else {
            $this->say('El paquete no pudo ser retirado del envío.');
        }
        $this->say('¿Quieres hacer algo más?');
        $this->mostrarOpcionesPaquetes();
    }

    /**
     * Permite Confirmar la información de los paquetes y adicionar información del envío, 
     * tal como la fecha del envio, el remitente, el destinatario y sus respectivas direcciones
     */
    private function confirmarEnvio() {
        $this->envio->refresh();
        if($this->envio->paquetes->isNotEmpty()) {
            $this->say('Entendido, continuemos.');
            $this->completarDatosEnvio();
        } else {
            $this->mostrarOpcionesEnvioVacio();
        }
    }

    /**
     * Solicita la información del envío antes de ser confirmado.
     */
    private function completarDatosEnvio(){
        $this->say('Ya casi está listo el envío. ');
        $this->say('Necesitamos la siguiente información: ');    
        $this->preguntarRemitente();
    }

    /**
     * Pregunta el nombre del Remitente.
     */
    private function preguntarRemitente() {
        $this->ask('Ingrese el nombre del remitente', function(Answer $respuesta) {
            if(!empty($respuesta->getText())) {
                $this->envio->remitente = $respuesta->getText();
                $this->preguntarDireccionRemitente();
            } else {
                $this->say('El Remitente ingresado no es válido. Por favor intenta nuevamente.');
                $this->preguntarRemitente();
            }
        });
    }

    /**
     * Pregunta la dirección del Remitente.
     */
    private function preguntarDireccionRemitente(){
        $this->ask('Ingrese la dirección del remitente', function(Answer $respuesta) {
            if(!empty($respuesta->getText())) {
                $this->envio->direccion_remitente = $respuesta->getText();
                $this->preguntarDestinatario();
                } 
            else{
                $this->say('La dirección ingresada no es válida. Por favor intenta nuevamente.');
                $this->preguntarDireccionRemitente();
            }
        });
    }

    /**
     * Pregunta el nombre del Destinatario.
     */
    private function preguntarDestinatario() {
        $this->ask('Ingrese el nombre del destinatario', function(Answer $respuesta) {
            if(!empty($respuesta->getText())) {
                $this->envio->destinatario = $respuesta->getText();
                $this->preguntarDireccionDestinatario();
            } 
            else{
                $this->say('El destinatario ingresado no es válido. Por favor intenta nuevamente.');
                $this->preguntarDestinatario();
            }
        });
    }

    /**
     * Pregunta la dirección del Destinatario. En caso de que sea correcto
     * prosigue a mostrar los datos del envio.
     */
    private function preguntarDireccionDestinatario(){
        $this->ask('Ingrese la dirección del destinatario', function(Answer $respuesta) {
            if(!empty($respuesta->getText())) {
                $this->envio->direccion_destinatario = $respuesta->getText();
                $this->envio->fecha_envio = new \DateTime();
                $this->say($this->resumenEnvio());
                if($this->envio->paquetes->isNotEmpty()) {
                    $this->say($this->listarPaquetes());                    
                } else {
                    $this->mostrarOpcionesEnvioVacio();
                }
                $this->confirmarResumen();
            } 
            else{
                $this->say('La dirección ingresada no es válida. Por favor intenta nuevamente.');
                $this->preguntarDireccionDestinatario();
            }
        });
    }

    /**
     * Muestra el resumen del envío para que el usuario lo confirme.
     */
    private function resumenEnvio(){
        $resumen="Resumen del envío: \n".
        'Remitente: ' . $this->envio->remitente . "\n" .
        'Dirección: ' . $this->envio->direccion_remitente . "\n" .
        'Destinatario: ' . $this->envio->destinatario . "\n" .
        'Dirección: ' . $this->envio->direccion_destinatario . "\n" .
        'Fecha y hora: ' . $this->envio->fecha_envio->format('d-m-Y H:i:s'). "\n" ;  
        return $resumen;         
    }

    /**
     * Muestra los paquetes asociados al envío.
     */
    private function listarPaquetes(){
        $lista="Paquetes registrados en el envío:\n";
        foreach($this->envio->paquetes as $paquete) {
            $lista.="\nCódigo paquete: " . $paquete->id . "\n" .
                        'Tipo: ' . $paquete->tipoPaquete->nombre . "\n" .
                        'Peso: ' . $paquete->peso . " kgs";
        }
        return $lista;
    }

    /**
     * Muestra el menú cuando no hay paquetes en un envío y se ha intentado confirmarlo.
     */
    private function mostrarOpcionesEnvioVacio() {
        $opciones = Question::create("No hay paquetes registrados en el envío actual. ¿Qué desea hacer?")
            ->addButtons([
                Button::create('Registrar paquete')->value(1),
                Button::create('Volver')->value(2),
            ]);
        $this->ask($opciones, function (Answer $respuesta) {
            if ($respuesta->isInteractiveMessageReply()) {
                $opcion = $respuesta->getValue();

                if($opcion == 1) {
                    $this->registrarPaquete();
                }
                
                if($opcion == 2) {
                    $this->say('Gracias por utilizar nuestros servicios.');
                    $this->validarEnvio();
                    $this->mostrarOpciones();
                }
            } else {
                $this->say('Por favor elige una opción de la lista.');
                $this->mostrarOpcionesEnvioVacio();
            }
        });
    }

    /**
     * Permite la confirmación de los paquetes previamente listados. Si falta algo, 
     * da la opción de regresar a las opciones de paquetes antes de confirmar el envio.
     */
    private function confirmarResumen(){
        $opciones = Question::create("¿Es correcta esta información?. ¿Está realmente seguro de realizar el envío?")
            ->addButtons([
                Button::create('¡Es correcta, estoy seguro!')->value(1),
                Button::create('No. Debo corregir')->value(2),
        ]);
        $this->ask($opciones, function (Answer $respuesta) {
            if ($respuesta->isInteractiveMessageReply()) {
                $opcion = $respuesta->getValue();

                if($opcion == 1) {
                    $this->procesarEnvio();    
                }
                
                if($opcion == 2) {
                    $this->mostrarOpcionesPaquetes();
                }
            } else {
                $this->say('Por favor elige una opción de la lista.');
            $this->confirmarResumen();
            }
        });    
    }

    /**
     * Guarda la información del envío en el sistema de manera persistente y confirma mediante
     * un código que corresponde al id del envío.
     */
    private function procesarEnvio(){
        $this->envio->codigo_envio = $this->envio->fecha_envio->getTimestamp() . '_' . strtoupper(substr($this->envio->remitente, 0, 3));
        $this->envio->save();

        $this->say('El envio se ha creado exitosamente.' . "\n" .
                   'Código de envío: ' . $this->envio->codigo_envio . "\n" .
                   'Fecha y hora del envío: ' . $this->envio->fecha_envio->format('d-m-Y H:i:s'));
    
        $this->enviarMensajeAdmin();
        $this->mostrarOpciones();
    }

    /**
     * Envía un mensaje al Administrador de la empresa de mensajería con los datos del envío.
     */
    private function enviarMensajeAdmin(){
        $usuariosAdmin = User::where("administrador",true)->get();
        foreach($usuariosAdmin as $usuario) {
            $this->bot->say($usuario->nombres." , hemos recibido un nuevo envio con los siguientes datos:", $usuario->codigo);    
            $this->bot->say($this->resumenEnvio(), $usuario->codigo);    
            $this->bot->say($this->listarPaquetes(), $usuario->codigo); 
        }        
    }


    /**
     * Muestra un listado con el historico de todos los envios 
     * para que la persona pueda ver toda la información del que selecciona.
     * En caso que solo se quiera mostrar los que no se han entregado (activos)
     * Se debe especificar en el parámetro.
     */
    private function historicoEnvios($activos=true){
        //Se consultan los envios asociados al usuario actual
        if($activos){
            $envios = Envio::where('usuario_id',$this->usuario->id)->where('estado','!=',Envio::ESTADO_ENTREGADO)->orderBy('estado')->get();
        }else{
            $envios = Envio::where('usuario_id',$this->usuario->id)->orderBy('estado')->get();
        }
        
        if($envios->isNotEmpty()) {
            $botones = [];
            
            foreach($envios as $envio) {
                $label = $envio->codigo_envio . ' - ' . $envio->estado;
                $botones[] = Button::create($label)->value($envio->id);
            }
            
            $opciones = Question::create("Seleccione uno de los envios siguientes para conocer su detalle: ")
            ->addButtons($botones);
            
            $this->ask($opciones, function (Answer $respuesta) {

                if ($respuesta->isInteractiveMessageReply()) {

                    $envioConsultar = Envio::find($respuesta->getValue());

                    $mensaje = "--------Información del Envío---- \n";
                    $mensaje .= "Código: {$envioConsultar->codigo_envio} \n";
                    $mensaje .= "Estado: {$envioConsultar->estado}\n";
                    $mensaje .= "Remitente: {$envioConsultar->remitente}\n";
                    $mensaje .= "Dirección Rem: {$envioConsultar->direccion_remitente}\n";
                    $mensaje .= "Destinatario: {$envioConsultar->destinatario}\n";
                    $mensaje .= "Dirección Des: {$envioConsultar->direccion_destinatario}\n";
                    $mensaje .= "Fecha: {$envioConsultar->fecha_envio}\n";
                    $mensaje .= "------Información de Paquetes---- \n";

                    $paquetes = Paquete::where('envio_id', $envioConsultar->id)->orderBy('id')->get();

                    foreach($paquetes as $paquete) {                        
                        $mensaje .= 'Tipo de Paquete : ' . $paquete->tipoPaquete->nombre . "\n";
                        $mensaje .= 'Peso: ' . $paquete->peso . "\n";
                        $mensaje .= "------------------------------ \n";
                    }

                    $this->say($mensaje);
                    $this->mostrarOpciones();


                } else {
                    $this->say('Por favor elige una opción de la lista.');
                    $this->historicoEnvios();
                }
            });
        } else {
            $this->say('No has realizado aún ningún envío.');
        }
    }
}

FROM php:7.2.11-apache
LABEL maintainer="brian.cardonas@autonoma.edu.co"
RUN apt-get update && apt-get install -y libldb-dev git zlib1g-dev
RUN apt-get update && docker-php-ext-install pdo_mysql mysqli zip
WORKDIR /
RUN echo 'ServerName localhost' >> /etc/apache2/apache2.conf
RUN a2enmod rewrite proxy proxy_fcgi headers
RUN service apache2 restart
WORKDIR /var/www/html/
CMD php artisan serve --host=0.0.0.0 --port=80
EXPOSE 80
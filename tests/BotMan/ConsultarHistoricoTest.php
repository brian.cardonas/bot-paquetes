<?php

namespace Tests\BotMan;

use Tests\TestCase;
use App\User;
use App\Envio;

class ConsultarHistoricoTest extends TestCase
{
    /**
     *Clase que permite realizar pruebas sobre la HU-06:
    * Consultar Histórico de Envíos
    * 
    * IMPORTANTE: Antes de ejecutar las pruebas se debe ejecutar el comando:
    * php artisan migrate:fresh --seed
    * 
    * @author Valentina Londoño Marin <vlondono@autonoma.edu.co>
    * @version 20190511
    * @see PHPUnit. Sitio web: https://phpunit.de/
     *
     * @return void
     */
    /**
     * Prueba consultar historico de envios.
     * Criterios de aceptación HU-06: 
     * - El bot debe mostrar un listado de los envíos del usuario con cualquier estado; es decir, PENDIENTE, RECOGIDO, ETC
     * - El bot debe mostrar toda la información del envio seleccionado en el criterio anterior: nombre y dirección del remitente, nombre y direccion del destinatorio, y los paquetes asociados con tipo y peso.
     */
    public function testConsutarHistoricoConEnvios() {

        $mensaje = "--------Información del Envío---- \n";
        $mensaje .= "Código: 20191704_4 \n";
        $mensaje .= "Estado: Recogido\n";
        $mensaje .= "Remitente: Marcel\n";
        $mensaje .= "Dirección Rem: Carrera 1 # 2 -3\n";
        $mensaje .= "Destinatario: Mama de Marcel\n";
        $mensaje .= "Dirección Des: Carrera 7 # 8 - 9\n";
        $mensaje .= "Fecha: 2019-04-01\n";
        $mensaje .= "------Información de Paquetes---- \n";  
        $mensaje .= "Tipo de Paquete : Ropa\n";
        $mensaje .= "Peso: 20\n";
        $mensaje .= "------------------------------ \n";
        $mensaje .= "Tipo de Paquete : Juguetes\n";
        $mensaje .= "Peso: 6\n";
        $mensaje .= "------------------------------ \n";

        $this->bot->receives('/inicioCliente')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            ->receivesInteractiveMessage(3)
            ->assertQuestion('Seleccione uno de los envios siguientes para conocer su detalle: ')
            ->receivesInteractiveMessage(4)
            ->assertReply($mensaje);
    }

    /**
     * Prueba el mensaje de fallback cuando un usuario envía texto a través del cliente,
     * en vez de utilizar los botones interactivos al seleccionar el envio del cual quiere saber el detalle.
     */
    public function testFallbackConsultaHistorico() {
        $this->bot->receives('/inicioCliente')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            ->receivesInteractiveMessage(3)
            ->assertQuestion('Seleccione uno de los envios siguientes para conocer su detalle: ')
            ->receives('Detalles de 3')
            ->assertReply('Por favor elige una opción de la lista.');
    }

    /**
     * Valida el mensaje de error cuando no se tiene ningún envio.
     * Criterio de aceptación:
     * - Si el usuario no tiene envíos el bot debe mostrar un mensaje 
     * informando que no hay envíos disponibles para consultar
     */
    public function testConsultarHistoricoSinEnvios() {
        $usuario = User::where('codigo',1)->first();
        Envio::where('usuario_id',$usuario->id)->delete();
        $this->bot->receives('/inicioCliente')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            ->receivesInteractiveMessage(3)         
            ->assertReply('No has realizado aún ningún envío.');
    }
}

<?php

namespace Tests\BotMan;

use Tests\TestCase;

/**
 * Clase que permite realizar pruebas sobre la HU-02:
 * Como cliente quiero retirar un paquete antes de formalizar el envío para que no sea enviado.
 * 
 * IMPORTANTE: Antes de ejecutar las pruebas se debe ejecutar el comando:
 * php artisan migrate:fresh --seed
 * 
 * @author Brian Cardona Salazar <brian.cardonas@autonoma.edu.co>
 * @version 20190506
 * @see PHPUnit. Sitio web: https://phpunit.de/
 */
class RetirarPaqueteTest extends TestCase {
    /**
     * Prueba retirar un paquete registrado del envío actual.
     * Criterios de aceptación HU-02: 
     * - El paquete debe quedar eliminado del envío actual
     * - El bot debe mostrar un mensaje de éxito al retirar el paquete
     */
    public function testRetirarPaqueteEnvioActual() {
        $this->bot->receives('/inicioCliente')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            ->receivesInteractiveMessage(1)
            ->assertReply('Entendido, comencemos.')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los paquetes')
            //Se registra el paquete que será eliminado posteriormente
            ->receivesInteractiveMessage(1)
            ->assertReply('Entendido, continuemos.')
            ->assertQuestion('Seleccione el tipo del paquete')
            ->receivesInteractiveMessage(1)
            ->assertReply("Ingrese el peso del paquete en Kilogramos \n Ejemplo (3.5) hasta 2 decimales")
            ->receives('1.2')
            ->assertReply('Paquete registrado con éxito.')
            //Se procede a retirar el paquete del envío actual
            ->assertReply('¿Quieres hacer algo más?')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los paquetes')
            ->receivesInteractiveMessage(2)
            ->assertQuestion('Seleccione el paquete que desea retirar del envío actual')
            ->receivesInteractiveMessage(10)
            ->assertReply('Paquete retirado del envío con éxito.');
    }

    /**
     * Prueba retirar un paquete que ya fue retirado del envío actual.
     */
    public function testRetirarPaqueteQueYaFueRetirado() {
        $this->bot->receives('/inicioCliente')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            ->receivesInteractiveMessage(1)
            ->assertReply('Entendido, comencemos.')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los paquetes')
            //Se registra el paquete que será eliminado posteriormente
            ->receivesInteractiveMessage(1)
            ->assertReply('Entendido, continuemos.')
            ->assertQuestion('Seleccione el tipo del paquete')
            ->receivesInteractiveMessage(2)
            ->assertReply("Ingrese el peso del paquete en Kilogramos \n Ejemplo (3.5) hasta 2 decimales")
            ->receives('2')
            ->assertReply('Paquete registrado con éxito.')
            //Se procede a retirar el paquete del envío actual
            ->assertReply('¿Quieres hacer algo más?')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los paquetes')
            ->receivesInteractiveMessage(2)
            ->assertQuestion('Seleccione el paquete que desea retirar del envío actual')
            ->receivesInteractiveMessage(11)
            ->assertReply('Paquete retirado del envío con éxito.')
            //Se procede a tratar de retirar nuevamente el paquete que ya fue retirado del envío actual
            ->assertReply('¿Quieres hacer algo más?')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los paquetes')
            ->receivesInteractiveMessage(2)
            ->assertQuestion('No hay paquetes registrados en el envío actual. ¿Qué desea hacer?');
    }

    /**
     * Prueba retirar un paquete de un envío que no tiene aún paquetes registrados.
     * Criterio de aceptación HU-02: 
     * - En caso de eliminar un paquete de un envío sin paquetes, el bot muestra un mensaje 
     * informando que no hay paquetes registrados
     */
    public function testRetirarPaqueteInexistente() {
        $this->bot->receives('/inicioCliente')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            ->receivesInteractiveMessage(1)
            ->assertReply('Entendido, comencemos.')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los paquetes')
            ->receivesInteractiveMessage(2)
            ->assertQuestion('No hay paquetes registrados en el envío actual. ¿Qué desea hacer?');
    }

    /**
     * Prueba el mensaje de fallback cuando un usuario envía texto a través del cliente,
     * en vez de utilizar los botones interactivos al retirar un paquete del envío.
     */
    public function testTextoRetirarPaquete() {
        $this->bot->receives('/inicioCliente')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            ->receivesInteractiveMessage(1)
            ->assertReply('Entendido, comencemos.')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los paquetes')
            //Se registra el paquete que será eliminado posteriormente
            ->receivesInteractiveMessage(1)
            ->assertReply('Entendido, continuemos.')
            ->assertQuestion('Seleccione el tipo del paquete')
            ->receivesInteractiveMessage(1)
            ->assertReply("Ingrese el peso del paquete en Kilogramos \n Ejemplo (3.5) hasta 2 decimales")
            ->receives('1.2')
            ->assertReply('Paquete registrado con éxito.')
            //Se procede a retirar el paquete del envío actual
            ->assertReply('¿Quieres hacer algo más?')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los paquetes')
            ->receivesInteractiveMessage(2)
            ->assertQuestion('Seleccione el paquete que desea retirar del envío actual')
            ->receives('elimina el primer paquete por favor')
            ->assertReply('Por favor elige una opción de la lista.')
            ->assertQuestion('Seleccione el paquete que desea retirar del envío actual');
    }
}

<?php

namespace Tests\BotMan;

use Tests\TestCase;
use App\User;
use App\Envio;

 /**
     *Clase que permite realizar pruebas sobre la HU-05:
    * Consultar Estado de un Envío
    * 
    * IMPORTANTE: Antes de ejecutar las pruebas se debe ejecutar el comando:
    * php artisan migrate:fresh --seed
    * 
    * @author Diego Eddye Hurtado <diego.hurtadoq@autonoma.edu.co>
    * @version 20190512
    * @see PHPUnit. Sitio web: https://phpunit.de/
     *
     * @return void
     */
class ConsultarEstadoTest extends TestCase
{
    /**
     * Prueba consultar estado de envios.
     * Criterios de aceptación HU-05: 
     * - El bot debe mostrar un resumen de los envíos del usuario con estado diferente a ENTREGADO
     * - El bot debe mostrar un mensaje con el estado del envío seleccionado en el criterio anterior
     */
    public function testConsultarEstadoConEnvios() {

        $mensaje = "--------Información del Envío---- \n";
        $mensaje .= "Código: 20191704_4 \n";
        $mensaje .= "Estado: Recogido\n";
        $mensaje .= "Remitente: Marcel\n";
        $mensaje .= "Dirección Rem: Carrera 1 # 2 -3\n";
        $mensaje .= "Destinatario: Mama de Marcel\n";
        $mensaje .= "Dirección Des: Carrera 7 # 8 - 9\n";
        $mensaje .= "Fecha: 2019-04-01\n";
        $mensaje .= "------Información de Paquetes---- \n";  
        $mensaje .= "Tipo de Paquete : Ropa\n";
        $mensaje .= "Peso: 20\n";
        $mensaje .= "------------------------------ \n";
        $mensaje .= "Tipo de Paquete : Juguetes\n";
        $mensaje .= "Peso: 6\n";
        $mensaje .= "------------------------------ \n";

        $this->bot->receives('/inicioCliente')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            ->receivesInteractiveMessage(2)
            ->assertQuestion('Seleccione uno de los envios siguientes para conocer su detalle: ')
            ->receivesInteractiveMessage(4)
            ->assertReply($mensaje);
    }

    /**
     * Prueba el mensaje de fallback cuando un usuario envía texto a través del cliente,
     * en vez de utilizar los botones interactivos al seleccionar el envio del cual quiere saber el detalle.
     */
    public function testFallbackConsultaEstado() {
        $this->bot->receives('/inicioCliente')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            ->receivesInteractiveMessage(3)
            ->assertQuestion('Seleccione uno de los envios siguientes para conocer su detalle: ')
            ->receives('Detalles de 3')
            ->assertReply('Por favor elige una opción de la lista.');
    }

    /**
     * Valida el mensaje de error cuando no se tiene ningún envio.
     * Criterio de aceptación:
     * - En caso de consultar el estado de los envíos, si el usuario no tiene 
     * envíos el bot debe mostrar un mensaje informando que no hay envíos disponibles para consultar
     * 
     * Este se valida con la prueba de historico. No se puede borrar pues no funcionarían
     * las otras pruebas
     */
}

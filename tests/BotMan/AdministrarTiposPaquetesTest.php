<?php

namespace Tests\BotMan;

use Tests\TestCase;


/**
 * Clase que permite realizar pruebas sobre la HU-07:
 * Como administrador de la empresa de mensajería quiero administrar los tipos de paquetes para mantener actualizados los tipos de paquetes que son permitidos enviar.
 * Criterios de aceptación HU-07: 
 * - El bot debe permitir listar los tipos de paquetes actuales.
 * - El bot debe permitir crear un nuevo tipo.
 * - El bot debe permitir inactivar o modificar el nombre de un tipo.
 * - El bot debe mostrar un mensaje de éxito al adicionar o modificar un tipo de paquete.
 * 
 * IMPORTANTE: Antes de ejecutar las pruebas se debe ejecutar el comando:
 * php artisan migrate:fresh --seed
 * 
 * @author Cristian Salazar <cristian.salazarp@autonoma.edu.co>
 * @version 20190510
 * @see PHPUnit. Sitio web: https://phpunit.de/
 */
class AdministrarTiposPaquetesTest extends TestCase {
    /**
     * Método de Prueba para cuando se actualiza el nombre de un tipo de paquete.
     */
    public function testActualizarTipoPaquete() {
        $this->bot->receives('/inicioAdmin')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            //Acción para administrar tipos de paquetes
            ->receivesInteractiveMessage(1)
            ->assertReply('Entendido, comencemos.')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los Tipos de paquetes:')
            //Acción para Listar paquetes existentes
            ->receivesInteractiveMessage(1)
            ->assertQuestion('Se encuentran los siguientes tipos de paquetes, seleccione el que desee administrar:')
            //Acción seleccionado en primer tipo de paquete
            ->receivesInteractiveMessage(1)
            ->assertQuestion('Seleccione la acción a realizar para el tipo de paquete:')
            //Acción para indicar que se actualiza el tipo de paquete
            ->receivesInteractiveMessage(1)
            ->assertReply('Ingrese el nuevo nombre para el tipo del paquete:')
            ->receives('Modificado 1')
            ->assertReply('Se ha modificado exitosamente el tipo de paquete.');
    }

    /**
     * Método de Prueba para cuando se actualiza el nombre de un tipo de paquete y no se ingresa el nombre.
     * Validación de obligatoriedad.
     */
    public function testActualizarTipoPaqueteErrorNombre() {
        $this->bot->receives('/inicioAdmin')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            //Acción para administrar tipos de paquetes
            ->receivesInteractiveMessage(1)
            ->assertReply('Entendido, comencemos.')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los Tipos de paquetes:')
            //Acción para Listar paquetes existentes
            ->receivesInteractiveMessage(1)
            ->assertQuestion('Se encuentran los siguientes tipos de paquetes, seleccione el que desee administrar:')
            //Acción seleccionado en primer tipo de paquete
            ->receivesInteractiveMessage(1)
            ->assertQuestion('Seleccione la acción a realizar para el tipo de paquete:')
            //Acción para indicar que se actualiza el tipo de paquete
            ->receivesInteractiveMessage(1)
            ->assertReply('Ingrese el nuevo nombre para el tipo del paquete:')
            ->receives('')
            ->assertReply('Debe ingresar un nombre válido, intentelo nuevamente.');
    }

    /**
     * Método de Prueba para cuando se actualiza el estado de un paquete a Activo.
     * Cómo dato de prueba se utiliza el segundo registro contenido en el seed TiposPaquetesSeeder.
     */
    public function testActivarTipoPaquete() {
        $this->bot->receives('/inicioAdmin')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            //Acción para administrar tipos de paquetes
            ->receivesInteractiveMessage(1)
            ->assertReply('Entendido, comencemos.')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los Tipos de paquetes:')
            //Acción para Listar paquetes existentes
            ->receivesInteractiveMessage(1)
            ->assertQuestion('Se encuentran los siguientes tipos de paquetes, seleccione el que desee administrar:')
            //Acción seleccionado en primer tipo de paquete
            ->receivesInteractiveMessage(2)
            ->assertQuestion('Seleccione la acción a realizar para el tipo de paquete:')
            //Acción para indicar que se actualiza el tipo de paquete
            ->receivesInteractiveMessage(2)
            ->assertReply('El nuevo estado del tipo de paquete es: Activo');
            
    }

    /**
     * Método de Prueba para cuando se actualiza el estado de un paquete a Inactivo.
     * Cómo dato de prueba se utiliza el primer registro contenido en el seed TiposPaquetesSeeder.
     */
    public function testInactivarTipoPaquete() {
        $this->bot->receives('/inicioAdmin')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            //Acción para administrar tipos de paquetes
            ->receivesInteractiveMessage(1)
            ->assertReply('Entendido, comencemos.')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los Tipos de paquetes:')
            //Acción para Listar paquetes existentes
            ->receivesInteractiveMessage(1)
            ->assertQuestion('Se encuentran los siguientes tipos de paquetes, seleccione el que desee administrar:')
            //Acción seleccionado en primer tipo de paquete
            ->receivesInteractiveMessage(1)
            ->assertQuestion('Seleccione la acción a realizar para el tipo de paquete:')
            //Acción para indicar que se actualiza el tipo de paquete
            ->receivesInteractiveMessage(2)
            ->assertReply('El nuevo estado del tipo de paquete es: Inactivo');
    }

    /**
     * Método de Prueba para cuando se crea un nuevo tipo de paquete.
     */
    public function testCrearTipoPaquete() {
        $this->bot->receives('/inicioAdmin')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            //Acción para administrar tipos de paquetes
            ->receivesInteractiveMessage(1)
            ->assertReply('Entendido, comencemos.')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los Tipos de paquetes:')
            //Acción para Crear tipo de paquete
            ->receivesInteractiveMessage(2)
            ->assertReply('Ingrese el nombre para el tipo del paquete:')
            ->receives('Tipo de Paquete PHP_UNIT')
            ->assertReply('Se ha creado el tipo de paquete Tipo de Paquete PHP_UNIT');
    }

    /**
     * Método de Prueba para cuando se crea un nuevo tipo de paquete y no se ingresa el nombre
     * Validación de obligatoriedad.
     */
    public function testCrearTipoPaqueteErrorNombre() {
        $this->bot->receives('/inicioAdmin')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            //Acción para administrar tipos de paquetes
            ->receivesInteractiveMessage(1)
            ->assertReply('Entendido, comencemos.')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los Tipos de paquetes:')
            //Acción para Crear tipo de paquete
            ->receivesInteractiveMessage(2)
            ->assertReply('Ingrese el nombre para el tipo del paquete:')
            ->receives('')
            ->assertReply('Debe ingresar un nombre, intentelo nuevamente.');
    }
}

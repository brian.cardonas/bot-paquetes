<?php

namespace Tests\BotMan;

use Tests\TestCase;

/**
 * Clase que permite realizar pruebas sobre la HU-09:
 * Como cliente quiero ser notificado via mensaje cuando haya una modificación del estado de mi envío para estar enterado del trámite del mismo.
 * 
 * IMPORTANTE: Antes de ejecutar las pruebas se debe ejecutar el comando:
 * php artisan migrate:fresh --seed
 * 
 * @author Valentina Londoño Marin <vlondono@autonoma.edu.co>
 * @version 20190508
 * @see PHPUnit. Sitio web: https://phpunit.de/
 */
class NotificarCliente extends TestCase
{
    /**
     * Prueba notificacion a cliente cuando se cambia el estado de un envío.
     * Criterios de aceptación HU-09: 
     * - El mensaje debe ser enviado después de que un administrador cambie el estado de un envío
     * - El mensaje debe saludar al usuario e informarle el cambio: El envio con código {codigo} ha tenido una novedad. El nuevo estado es {estado}.
     */
    public function testNotificarCliente() {
        $mensaje = "Se ha actualizado el envío 20191704_2 con los siguientes datos: \n";
        $mensaje = "--------Información actual del Envío---- \n";
        $mensaje .= "Nuevo estado: En camino\n";
        $mensaje .= "Remitente: Valentina\n";
        $mensaje .= "Dirección Rem: Cra 6C #15A-21\n";
        $mensaje .= "Destinatario: Valentina Londoño Marín\n";
        $mensaje .= "Dirección Des: CL 15 24 52 San Antonio\n";
        $mensaje .= "Fecha del envío: 2019-04-01\n";
        $mensaje .= "---------------------------------------";

        $this->bot->receives('/inicioAdmin')
            //->assertReply('Ingresa el token que confirma que eres admin')
            //->receives('@123')
            ->assertReply('Bienvenido Marcel al SRSP.')
            ->assertQuestion('¿Qué acción desea realizar?')
            //Opción para administrar envios
            ->receivesInteractiveMessage(2)
            ->assertReply('Entendido, comencemos.')
            ->assertQuestion('Seleccione alguna de las siguientes opciones para los envíos')
            //Modificar estado
            ->receivesInteractiveMessage(2)
            ->assertReply("Ingrese el código del envío a actualizar")            
            ->receives('20191704_2')
            ->assertReply($mensaje)
            ->assertReply('Buen día Sr(a) Valentina')
            ->assertReply($mensaje);
    }   
}

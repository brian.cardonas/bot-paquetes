<?php
use App\Http\Controllers\BotManController;
use App\Conversations\ConversacionCliente;
use App\Conversations\ConversacionAdministrador;
use BotMan\BotMan\Messages\Incoming\Answer;

$botman = resolve('botman');

$botman->hears('/ayuda|ayuda|/start', function ($bot) {
    $ayuda = ['/ayuda' => 'Muestra este mensaje de ayuda',
        '/inicioCliente' => 'Muestra las opciones que tiene un Cliente',
        '/inicioAdmin' => 'Muestra las opciones que tiene un Administrador',
        '/acerca' => 'Muestra información del desarrollo del bot',
    ];
    
    $bot->reply("Los comandos disponibles son:");
    foreach($ayuda as $key => $value) {
        $bot->reply($key . ": " . $value);
    }
});

/*Mensaje para acerca de*/
$botman->hears('/acerca|acerca', function ($bot) {
    $mensaje = "Este bot fue desarrollado por:\n".
        "Valentina Londoño Marín <vlondono@autonoma.edu.co>\n".
        "Cristian Alexander Salazar Perdomo <cristian.salazarp@autonoma.edu.co>\n".
        "Diego Eddye Hurtado Quintero <diego.hurtadoq@autonoma.edu.co>\n".
        "Brian Cardona Salazar <brian.cardonas@autonoma.edu.co>\n".
        "Durante el módulo de Procesos Ágiles de Desarrollo de Software\n".
        "Universidad Autónoma de Manizales - 2019.";
    $bot->reply($mensaje);
});

/*Inicio de conversación de cliente*/
$botman->hears('/inicioCliente', function ($bot) {
    $usuarioBot = $bot->getUser();
    $id = $usuarioBot->getId();
    //Creación/actualización del usuario
    $nombres = $usuarioBot->getFirstName() ?: "Desconocido";
    $usuario = App\User::firstOrNew(array(
        'codigo' => $id,
        'nombres' => $nombres,
    ));
    $usuario->save();
    $bot->reply("Bienvenido $nombres al SRSP.");
    $bot->startConversation(new ConversacionCliente());
})->stopsConversation();

/*Inicio de conversación de admininistrador*/
$botman->hears('/inicioAdmin', function ($bot) {    
    $bot->startConversation(new ConversacionAdministrador());    
})->stopsConversation();

$botman->fallback(function ($bot) {
    $bot->reply("No entiendo que quieres decir, vuelve a intentarlo o escribe /ayuda 
        para conocer nuestros comandos.");
});
